# smooster contact ajax example

You can use this example to see one solution how to create a contactform with ajax in smooster.

## Folder structure
* /html contains the project related files (html, css, js, images, ...)

## Installation

### clone git

    $ git clone git@bitbucket.org:smooster/smo-html-contact-ajax-example.git

  or just download it via [Downloads](https://bitbucket.org/smooster/smo-html-contact-ajax-example/downloads)

### deploy to smooster

Use the [smooster CLI](https://bitbucket.org/smooster/smooster) to create your own demo in smooster.

replace SITE_ID with the site id of the site you created in smooster

    $ smooster setup SITE_ID

then deploy the example

    $ smooster deploy initial

this will create the media assets, templates and pages. But before you can check it, you need to make some further adjustments

1. login to smooster CMS -> select your new site
2. create a form (via more & form)
3. copy the id from the form (in the url)
4. click more & templates
5. select the "index" template -> click on edit
8. save the template


## Usage

  Use the html/index.html as your first template file, if needed you can add more html files in /html.


## Wishlist
* None so far

## Changelog

###0.1.0
* Basic folder and file structure

## Contributing

1. Fork it ( https://bitbucket.org/smooster/smo-html-contact-ajax-example/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
