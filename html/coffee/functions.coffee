# Just delete the lines below, which are not needed #
# BEGIN CONFIG #
# @codekit-prepend "lib/smo-mobile-nav.coffee"
# END CONFIG #

$(window).load ->
  $("body").removeClass("preload")

$(document).ready ->

  loadFormHandlerIndividualError = ->
    #Ladda.bind( 'input[type=submit]' );
    Ladda.bind( '.ladda-button', { timeout: 60000 } )

    $('.contactform-individual').submit (e) ->
      e.preventDefault()
      form_data =
        f:
          email: $('[name="f[E-Mail]"]',@).val()
          name: $('[name="f[Name]"]',@).val()
          message: $('[name="f[Notitz]"]',@).val()
      form_data_json = JSON.stringify(form_data)

      this_form = @

      settings = $.extend(

        # These are the defaults.
        selector_set_error_class: '.field_row'
        error_class: "field_error"
        error_msg_class: "error_msg"
      )


      valide = true
      $(".#{settings.error_msg_class}").hide()

      $('[required]', @).each (index, value) ->
        if $(value).is('input[type=checkbox]') || $(value).is('input[type=radio]')
          unless $(value).prop('checked')
            if $(value).closest(settings.selector_set_error_class).length > 0
              $(value).closest(settings.selector_set_error_class).addClass(settings.error_class)
            else
              $(value).addClass(settings.error_class)
          else
            if $(value).closest(settings.selector_set_error_class).length > 0
              $(value).closest(settings.selector_set_error_class).removeClass(settings.error_class)
            else
              $(value).removeClass(settings.error_class)
        else
          if $(value).val() == ""
            if $(value).closest(settings.selector_set_error_class).length > 0
              $(value).closest(settings.selector_set_error_class).addClass(settings.error_class)
            else
              $(value).addClass(settings.error_class)
          else
            if $(value).closest(settings.selector_set_error_class).length > 0
              $(value).closest(settings.selector_set_error_class).removeClass(settings.error_class)
            else
              $(value).removeClass(settings.error_class)

      regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/

      $('[type=email]', @).each (index, value) ->
        unless regex.test($(value).val())
          if $(value).closest(settings.selector_set_error_class).length > 0
            $(value).closest(settings.selector_set_error_class).addClass(settings.error_class)
          else
            $(value).addClass(settings.error_class)
        else
          if $(value).closest(settings.selector_set_error_class).length > 0
            $(value).closest(settings.selector_set_error_class).removeClass(settings.error_class)
          else
            $(value).removeClass(settings.error_class)
      $('[type=email]', @).each (index, value) ->
        valide = false unless regex.test($(value).val())

      $('[required]', @).each (index, value) ->
        if $(value).is('input[type=checkbox]') || $(value).is('input[type=radio]')
          valide = false unless $(value).prop('checked')
        else
          valide = false if $(value).val() == ""

      unless valide
        $(".#{settings.error_msg_class}", @).fadeIn()
        e.preventDefault()
        $(".mail-error").fadeIn 300
        window.setTimeout((->
          Ladda.stopAll();
          ), 1000)
        return false
      else
        #submit data to server
        $.ajax
          type: 'POST',
          beforeSend: (xhr) ->
            xhr.setRequestHeader 'Content-Type', 'application/json'
            xhr.setRequestHeader 'Accept', 'application/json'
            return
          #contentType: "application/json; charset=utf-8",
          dataType: "JSON",
          url: $(@).attr('action'),
          processData: false
          contentType: 'application/json'
          data: form_data_json
          success: (data) ->
            window.setTimeout((->
              Ladda.stopAll();
              $("button.ladda-button").fadeOut 300, ->
                if $(".mail-error").is(":visible")
                  $(".mail-error").fadeOut 300, ->
                    $(".mail-success").fadeIn 300
                else
                  $(".mail-success").fadeIn 300
              ), 1000)
          error: (jqHXR, textStatus, errorThrown) ->

            window.setTimeout((->
              Ladda.stopAll();
              $(".mail-error").fadeIn 300
              ), 1000)
            console.log "Error"
            console.log textStatus
            console.log errorThrown


  loadFormHandlerBrowserError = ->
    #Ladda.bind( 'input[type=submit]' );
    Ladda.bind( '.ladda-button', { timeout: 60000 } )

    $('.contactform-browser').submit (e) ->
      e.preventDefault()
      form_data =
        f:
          email: $('[name="f[E-Mail]"]',@).val()
          name: $('[name="f[Name]"]',@).val()
          message: $('[name="f[Notitz]"]',@).val()
      form_data_json = JSON.stringify(form_data)

      this_form = @

      #submit data to server
      $.ajax
        type: 'POST',
        beforeSend: (xhr) ->
          xhr.setRequestHeader 'Content-Type', 'application/json'
          xhr.setRequestHeader 'Accept', 'application/json'
          return
        #contentType: "application/json; charset=utf-8",
        dataType: "JSON",
        url: $(@).attr('action'),
        processData: false
        contentType: 'application/json'
        data: form_data_json
        success: (data) ->
          window.setTimeout((->
            Ladda.stopAll();
            $("button.ladda-button").fadeOut 300, ->
              if $(".mail-error").is(":visible")
                $(".mail-error").fadeOut 300, ->
                  $(".mail-success").fadeIn 300
              else
                $(".mail-success").fadeIn 300
            ), 1000)
        error: (jqHXR, textStatus, errorThrown) ->

          window.setTimeout((->
            Ladda.stopAll();
            $(".mail-error").fadeIn 300
            ), 1000)
          console.log "Error"
          console.log textStatus
          console.log errorThrown

  loadFormHandlerIndividualError()
  loadFormHandlerBrowserError()
